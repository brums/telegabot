# coding=utf-8
import telebot
import config
from telebot import types
from db_connection import connect
import random
from remind import input_date, proсess_date
from calculation import calc, input_parameters
from db import view_exercises, output_excers, exercise, add_exercise, \
    search_food, count_ccal, add_new_food_base
from db import output_recepy, name_recp, name_ingrds

client_dict = {}
client_keys = ['id', 'gen', 'height', 'age', 'activity', 'weight']
for x in client_keys:
    client_dict[x] = 0

timezone_dict = {}
timezone_keys = ['hour']
for x in timezone_keys:
    timezone_dict[x] = 0

food_dict = {}
food_keys = ['name', 'prot', 'fat', 'carboh', 'ccal']
for x in food_keys:
    food_dict[x] = 0

ex_dict = {}
ex_keys = ['excers']
for x in ex_keys:
    ex_dict[x] = 0

cursor, db = connect()

bot = telebot.TeleBot(
    config.TOKEN)  # связываю код с ботом по его личному токену


class FoodClass:  # продукт для внесения в базу
    def __init__(self, prod, prot, fat, carboh, ccal):
        self.name = prod
        self.prot = prot
        self.fat = fat
        self.carboh = carboh
        self.ccal = ccal


@bot.message_handler(
    commands=['start', 'help'])  # пользователь нажимает старт или помощь
def start_message(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button_calculation = types.KeyboardButton('Расчет')
    button_workout = types.KeyboardButton('Тренировка')
    button_food = types.KeyboardButton('Питание')
    button_motivation = types.KeyboardButton('Мотивация')
    button_remind = types.KeyboardButton('Напомни')
    button_training = types.KeyboardButton('Тренировка на сегодня')
    button_recep = types.KeyboardButton('ПП рецепты')
    markup.add(button_calculation, button_food, button_motivation,
               button_workout, button_remind, button_training,
               button_recep)

    bot.send_message(message.chat.id,
                     'Привет, {0.first_name}! Я - \n<b>{1.first_name}</b>, '
                     'бот созданный чтобы помочь тебе.\n'
                     'Я могу рассчитать твою каллорийность, подобрать тренировки, '
                     'помогать тебе вести дневник питания.\n'
                     'А еще я могу мотивировать тебя и напоминать о тренировках :)\n'
                     'Чтобы я понял что делать нажми нужную кнопку на клавиатуре. '
                     .format(message.from_user, bot.get_me()),
                     parse_mode='html', reply_markup=markup)


@bot.message_handler(content_types=['text'])
def main_action(message):
    """
    Функция обрабатывает выбор пользователя
    :param message: сообщение пользователя
    :return: Ничего не возвращает, вызывает другие функции или выводит кнопки
    """
    if message.text == 'Расчет':
        gender(message)

    if message.text == 'Питание':
        nutrition(message)

    if message.text == 'Мотивация':
        markup = types.InlineKeyboardMarkup(row_width=2)
        button_n = types.InlineKeyboardButton("Обычная ", callback_data='norm')
        button_h = types.InlineKeyboardButton("Хардовая ",
                                              callback_data='hard')
        markup.add(button_n, button_h)
        bot.send_message(message.chat.id, 'Какую мотивациию вы предпочитаете?',
                         reply_markup=markup)

    if message.text == 'Тренировка':
        markup = types.InlineKeyboardMarkup(row_width=5)
        button_2 = types.InlineKeyboardButton("Руки", callback_data='h')
        button_3 = types.InlineKeyboardButton("Пресс", callback_data='p')
        button_4 = types.InlineKeyboardButton("Спина", callback_data='b')
        button_5 = types.InlineKeyboardButton("Ягодицы", callback_data='a')
        button_6 = types.InlineKeyboardButton("Ноги", callback_data='l')
        markup.add(button_2, button_3, button_4, button_5, button_6)
        bot.send_message(message.chat.id, 'Выберите группу мышц.',
                         reply_markup=markup)

    if message.text == 'Тренировка на сегодня':
        list_exercises_today(message)

    if message.text == 'Напомни':
        remind(message)

    if message.text == 'ПП рецепты':
        markup = types.InlineKeyboardMarkup(row_width=6)
        button_b = types.InlineKeyboardButton("Завтраки", callback_data='brf')
        button_l = types.InlineKeyboardButton("Обеды", callback_data='lnc')
        button_d = types.InlineKeyboardButton("Ужины", callback_data='din')
        markup.add(button_b, button_l, button_d)
        bot.send_message(message.chat.id, 'Выберите.', reply_markup=markup)


@bot.callback_query_handler(func=lambda c: c.data == 'brf' or c.data == 'lnc' or c.data == 'din')
def recepies(c):
    """Функция вывода всех блюд.
        Выводит завтраки, обеды и ужины в зависимости от нажатой кнопки, которые есть в базе.
       Args:
           c: кнопка, нажатая пользователем
       Returns:
           ничего не возвращает"""
    if c.data == 'brf':
        sql = "SELECT name FROM recepies WHERE kind = 'завтрак'"
        rec_list = output_recepy(sql)
        bot.send_message(c.message.chat.id, "\n".join(rec_list))
    if c.data == 'lnc':
        sql = "SELECT name FROM recepies WHERE kind = 'обед'"
        rec_list = output_recepy(sql)
        bot.send_message(c.message.chat.id, "\n".join(rec_list))
    if c.data == 'din':
        sql = "SELECT name FROM recepies WHERE kind = 'ужин'"
        rec_list = output_recepy(sql)
        bot.send_message(c.message.chat.id, "\n".join(rec_list))
    msg = bot.send_message(c.message.chat.id,
                           'Введите название понравившегося блюда')
    bot.register_next_step_handler(msg, look_recepy)


def look_recepy(message):
    """Функция вывода рецепта введенного блюда.
     Выводит рецепт и ингридиенты, если блюдо есть в базе.
    :param message: сообщение пользователя
    :return: ничего не возвращает"""
    rec_chce = message.text.lower()
    recpy = name_ingrds(rec_chce)
    recp = name_recp(rec_chce)
    if recp and recpy:
        bot.send_message(message.chat.id, recpy)
        bot.send_message(message.chat.id, recp)
    else:
        msg = bot.send_message(message.chat.id,
                               "Такого блюда нет. Повторите ввод.")
        bot.register_next_step_handler(msg, look_recepy)



@bot.callback_query_handler(func=lambda c: c.data == 'norm' or c.data == 'hard')
def norm_motivation(c):
    if c.data == 'norm':
        sql = "SELECT motivation FROM motivation WHERE number LIKE %s "
        num = random.randint(1, 13)
        val = (num,)
        cursor.execute(sql, val)
        your_motivation = cursor.fetchall()
        bot.send_message(c.message.chat.id, your_motivation)

    if c.data == 'hard':
        sql = "SELECT motivation FROM angry_motivation WHERE number LIKE %s "
        num = random.randint(1, 14)
        val = (num,)
        cursor.execute(sql, val)
        your_motivation = cursor.fetchall()
        bot.send_message(c.message.chat.id, your_motivation)


@bot.callback_query_handler(
    func=lambda c: c.data == 'h' or c.data == 'p' or c.data == 'b' or c.data == 'a' or c.data == 'l')
def exercises(c):
    """Функция вывода всех упражнений.
            Выводит на разные группы мышц, которые есть в базе.
           :param c: кнопка, нажатая пользователем
           :return: ничего не возвращает"""
    if c.data == 'h':
        sql = "SELECT title FROM exercises WHERE muscle_group = 'руки'"
        ex_list = output_excers(sql)
        bot.send_message(c.message.chat.id, "\n".join(ex_list))

    if c.data == 'p':
        sql = "SELECT title FROM exercises WHERE muscle_group = 'пресс' "
        ex_list = output_excers(sql)
        bot.send_message(c.message.chat.id, "\n".join(ex_list))

    if c.data == 'b':
        sql = "SELECT title FROM exercises WHERE muscle_group = 'спина' "
        ex_list = output_excers(sql)
        bot.send_message(c.message.chat.id, "\n".join(ex_list))

    if c.data == 'a':
        sql = "SELECT title FROM exercises WHERE muscle_group = 'ягодицы' "
        ex_list = output_excers(sql)
        bot.send_message(c.message.chat.id, "\n".join(ex_list))

    if c.data == 'l':
        sql = "SELECT title FROM exercises WHERE muscle_group = 'ноги' "
        ex_list = output_excers(sql)
        bot.send_message(c.message.chat.id, "\n".join(ex_list))

    msg = bot.send_message(c.message.chat.id,
                           'Введите название понравившегося упражнения')
    bot.register_next_step_handler(msg, definition)


def definition(message):
    """Функция вывода информации об упражнении.
         Выводит картинку, описание упраженения, если оно есть в базе.
        :param message: сообщение пользователя
        :return: ничего не возвращает"""
    exc_chce = message.text.lower()
    ex_dict['excers'] = exc_chce
    excr = exercise(exc_chce)

    if excr:
        bot.send_message(message.chat.id, excr)
        exc_chce = exc_chce.replace('"', '')
        exc_chce = exc_chce.replace(' ', '%20')
        bot.send_photo(message.chat.id,
                       "https://gitlab.com/brums/telegabot/-/raw/master/excr/" + exc_chce + ".jpg")

        markup = types.InlineKeyboardMarkup(row_width=2)
        button_yes = types.InlineKeyboardButton("Да", callback_data='y')
        button_no = types.InlineKeyboardButton("Нет", callback_data='n')
        markup.add(button_yes, button_no)

        bot.send_message(message.chat.id,
                         'Хотите ли сегодня делать это упражнение?',
                         reply_markup=markup)

    else:
        msg = bot.send_message(message.chat.id,
                               "Такого упражнения нет. Повторите ввод.")
        bot.register_next_step_handler(msg, definition)


@bot.callback_query_handler(func=lambda c: c.data == 'y' or c.data == 'n')
def yes_or_no(c):
    if c.data == 'y':
        msg = bot.send_message(c.message.chat.id,
                               "Введите количество подходов и повторов через ',' .")
        bot.register_next_step_handler(msg, training)

    if c.data == 'n':
        bot.send_message(c.message.chat.id, "Упражнение не добавлено.")


def training(message):
    """Функция добавления упражнения в сегодняшние упражнения.
         Добавляет упражнение в базу, если корректно введены кол-во подходов и повторений.
        :param message: сообщение пользователя
        :return: ничего не возвращает"""
    user_id = message.from_user.id
    text = message.text
    res = add_exercise(text)
    if res == 1:
        msg = bot.send_message(message.chat.id,
                               "Неправильный ввод. Введите количество подходов и повторов через ',' еще раз. ")
        bot.register_next_step_handler(msg, training)

    else:
        amount, repeat = add_exercise(text)
        string = ex_dict['excers'] + " подходов: " + str(amount) + " повторений: " + str(repeat)
        sql = "INSERT INTO personal_exc (user_id, exc) VALUES (%s, %s)"
        val = (user_id, string)
        cursor.execute(sql, val)
        db.commit()
        bot.send_message(message.chat.id, "Упражение записано")
        return


def delete_excers(message):
    """Функция удаление упражнений.
         Удаляет все сегодняшние упражнения из базы.
        :param message: сообщение пользователя
        :return: ничего не возвращает"""
    user_id = message.from_user.id
    ans = message.text.lower()

    if ans == 'да':
        sql = "DELETE FROM personal_exc WHERE user_id LIKE %s"
        val = (user_id,)
        cursor.execute(sql, val)
        db.commit()
        bot.send_message(message.chat.id, 'Все было удалено.')

    elif ans == 'нет':
        bot.send_message(message.chat.id, 'Все осталось как прежде.')

    else:
        bot.send_message(message.chat.id, 'Что-то пошло не так.')
        return


@bot.message_handler(content_types=['text'])
def gender(message):
    """
    Функция выводит кнопки, вызывает другую функцию и выводит кнопки
    :param message: сообщение пользователя
    :return: ничего не возвращает
    """
    markup = types.InlineKeyboardMarkup(row_width=2)
    button_m = types.InlineKeyboardButton("Мужчина", callback_data='М')
    button_w = types.InlineKeyboardButton("Женщина", callback_data='Ж')
    markup.add(button_w, button_m)
    bot.send_message(message.chat.id, 'Ваш пол?', reply_markup=markup)


@bot.callback_query_handler(func=lambda c: c.data == 'М' or c.data == 'Ж')
def ask_gender(c):
    """
    Функция обрабатывает выбор пользователя, вызывает другую функцию и выводит сообщение
    :param c: кнопка
    :return: ничего не возвращает
    """
    if c.data == 'М':
        client_dict['gen'] = 5
    else:
        client_dict['gen'] = -161
    mes = bot.send_message(c.message.chat.id,
                           'Введите ваш рост, вес и возраст через запятую.'
                           'Для выхода из расчета напишите отмена.')
    bot.register_next_step_handler(mes, ask_parameters)


def ask_parameters(message):
    """
    Функция обрабатывает сообщение пользователя, вызывает другую функцию и
    выводит сообщения и кнопки. Проверяет корректность введенного значения.
    :param message: сообщение пользователя
    :return: ничего не возвращает
    """
    text = message.text.lower()
    if text == 'отмена':
        bot.send_message(message.chat.id, 'Отменено.')
        return

    else:
        client_dict['id'] = message.from_user.id
        res = input_parameters(message.text)

        if res == 1:
            mes = bot.send_message(message.chat.id,
                                   'Введено не число, либо введены не все значения. '
                                   'Вы можете попробовать еще раз или выйти из расчета, '
                                   'для этого напишите отмена.')
            bot.register_next_step_handler(mes, ask_parameters)

        else:
            height, weight, age = input_parameters(message.text)

            if height > 250:
                mes = bot.send_message(message.chat.id,
                                       'Людей такого роста не бывает, введите значение заново.')
                bot.register_next_step_handler(mes, ask_parameters)

            if weight > 200:
                mes = bot.send_message(message.chat.id,
                                       'Слишком большое число, введите значение заново.')
                bot.register_next_step_handler(mes, ask_parameters)

            if age > 70:
                mes = bot.send_message(message.chat.id,
                                       'Столько не живут :). Введите заново.')
                bot.register_next_step_handler(mes, ask_parameters)

            client_dict['height'] = height
            client_dict['weight'] = weight
            client_dict['age'] = age

        markup = types.InlineKeyboardMarkup(row_width=4)
        button_1 = types.InlineKeyboardButton("Минимальный", callback_data='1')
        button_2 = types.InlineKeyboardButton("Легкий", callback_data='2')
        button_3 = types.InlineKeyboardButton("Средний", callback_data='3')
        button_4 = types.InlineKeyboardButton("Высокий", callback_data='4')
        markup.add(button_1, button_2, button_3, button_4)
        bot.send_message(message.chat.id,
                         'Выберите уровень активности. Минимальный - сидячая работа и отсутствие спорта.\n '
                         'Легкий - упражнения 2-3 р/нед, длительные прогулки.\n'
                         'Средний - спорт до 4-5 р/нед.\n'
                         'Высокий - тяжелый физический труд и тренировки каждый день',
                         reply_markup=markup)


@bot.callback_query_handler(func=lambda c: c.data == '1' or c.data == '2' or c.data == '3' or c.data == '4')
def action_purpose(c):
    """
    Функция обрабатывает сообщение пользователя, вызывает другую функцию и
    выводит сообщение/кнопки
    :param c: нажатая кнопка
    :return: ничего не возвращает
    """
    if c.data == 'Минимальный':
        activity = 1.2
    elif c.data == 'Легкий':
        activity = 1.4
    elif c.data == 'Средний':
        activity = 1.6
    else:
        activity = 1.8
    client_dict['activity'] = activity

    markup = types.InlineKeyboardMarkup(row_width=3)
    button_s = types.InlineKeyboardButton("Похудение", callback_data='s')
    button_m = types.InlineKeyboardButton("Поддержание", callback_data='m')
    button_w = types.InlineKeyboardButton("Набор массы", callback_data='w')
    markup.add(button_s, button_m, button_w)
    bot.send_message(c.message.chat.id, 'Ваша цель', reply_markup=markup)


@bot.callback_query_handler(
    func=lambda c: c.data == 's' or c.data == 'w' or c.data == 'm')
def calculation(c):
    """
    Функция обрабатывает выбор пользователя,  добавляет
    даннные в базу или обновляет их
    :param c: нажатая кнопка
    :return: ничего не возвращает
    """
    weight = client_dict['weight']
    height = client_dict['height']
    age = client_dict['age']
    gen = client_dict['gen']
    activity = client_dict['activity']

    if c.data == 'w':
        val = 1
        all_ccal, proteins, fats, carb = calc(val, height, weight, age, gen,
                                              activity)
    elif c.data == 's':
        val = 2
        all_ccal, proteins, fats, carb = calc(val, height, weight, age, gen,
                                              activity)
    else:
        val = 3
        all_ccal, proteins, fats, carb = calc(val, height, weight, age, gen,
                                              activity)

    user_id = client_dict['id']

    try:
        sql = "INSERT INTO users (user_id, ccal, proteins, fats, carb) VALUES (%s, %s, %s, %s, %s)"
        val = (user_id, all_ccal, proteins, fats, carb)
        cursor.execute(sql, val)
        db.commit()

    except Exception:
        sql = "UPDATE users SET ccal = %s, proteins = %s, fats = %s, carb = %s WHERE user_id = %s"
        val = (all_ccal, proteins, fats, carb, user_id)
        cursor.execute(sql, val)
        db.commit()

    answer = 'Ваша норма ккал: ' + str(all_ccal) + "\n КБЖУ: " + str(
        proteins) + "/" + str(fats) + "/" + str(carb)
    bot.send_message(c.message.chat.id, text=answer)


@bot.message_handler(content_types=['text'])
def nutrition(message):
    """Функция вывода кнопок, при нажатии кнопки 'Питание'"""
    markup = types.InlineKeyboardMarkup(row_width=2)
    button_add = types.InlineKeyboardButton("Добавить продукт",
                                            callback_data='+')
    button_eat = types.InlineKeyboardButton("Что я съел?", callback_data='?')
    markup.add(button_add, button_eat)
    bot.send_message(message.chat.id, 'Что вы хотите сделать?',
                     reply_markup=markup)


@bot.callback_query_handler(
    func=lambda c: c.data == '+')  # нажали добавить продукт
def add_food(c):
    """Функция обработки кнопки 'Добавить продукт'"""
    msg = bot.send_message(c.message.chat.id, 'Введите название продукта.')
    bot.register_next_step_handler(msg, add_food_step)


def add_food_step(message):  # ищем продукт в базе
    """Функция поиска продукта в базе.
         Выводит информацию о продукте и запрашивает количество съеденных грамм, если продукт есть в базе.
         Если нет, то вывводит сообщение 'Внести данный продукт в базу?'
    :param message: сообщение пользователя
    :return: ничего не возвращает """
    product = message.text.lower()
    if product.replace(' ', '').isalpha():
        products = search_food(product)
        if products != 0:
            msg = bot.send_message(message.chat.id,
                                   products + "\n Сколько грамм вы съели?")
            prod, protein, fats, carbohyd, ccal = products.split(',')
            food_dict['name'] = prod
            food_dict['prot'] = protein
            food_dict['fat'] = fats
            food_dict['carboh'] = carbohyd
            food_dict['ccal'] = ccal
            bot.register_next_step_handler(msg, count_callories)

        else:  # нет в базе
            msg = bot.send_message(message.chat.id,
                                   "У нас нет такого продукта \n "
                                   "Внести данный продукт в базу (да/нет)?")
            bot.register_next_step_handler(msg, yes_no_step)
    else:  # неправильно введено название продукта
        msg = bot.send_message(message.chat.id,
                               "Неправильный ввод. Введите название продукта еще раз")
        bot.register_next_step_handler(msg, add_food_step)


def count_callories(message):
    """Функция подсчета калорий.
             Выводит информацию о съеденном КБЖУ.
    :param message: нажатая кнопка
    :return: ничего не возвращает"""

    product = FoodClass(food_dict['name'], food_dict['prot'], food_dict['fat'], food_dict['carboh'], food_dict['ccal'])
    gr = message.text

    if gr.isdigit():
        gr = int(gr)
        user_id = message.from_user.id
        name, prot, fat, carboh, ccal = count_ccal(product.name, product.prot,
                                                   product.fat, product.carboh,
                                                   product.ccal, gr)

        sql = "INSERT INTO together (`user_id`, `prodn`, `prot`, `fats`, `carbh`, `call`) \
                                                              VALUES (%s, %s, %s, %s, %s, %s)"
        val = (user_id, name, str(prot), str(fat), str(carboh), str(ccal),)
        cursor.execute(sql, val)
        db.commit()
        bot.send_message(message.chat.id,
                         'вы съели : {0}, \n белков: {1} \n жиров: {2} \n углеводов: {3} \n калорий: {4}'
                         .format(name, prot, fat, carboh, ccal))
    else:
        msg = bot.send_message(message.chat.id,
                               "Вы ввели не число. \n Введите граммовку заново")
        bot.register_next_step_handler(msg, count_callories)


def yes_no_step(message):  # запрос на добавление продукта в базу
    """Функция запроса на добавление продукта в базу.
    :param message: сообщение пользователя
    :return: ничего не возвращает"""
    text = message.text.lower()
    if text == 'да':
        msg = bot.send_message(message.chat.id, "Введите название, белки,"
                                                " жиры , углеводы, каллораж через ',' (расчет на сто грамм). ")
        bot.register_next_step_handler(msg, add_new_food_step)

    elif text == 'нет':
        return

    else:
        msg = bot.send_message(message.chat.id,
                               "Неправильный ответ. Пожалуйста, введите его заново.")
        bot.register_next_step_handler(msg, yes_no_step)


def add_new_food_step(message):  # добавляем продукт в базу
    """Функция на добавление продукта в базу.
    Добавляет новый продукт в базу, если все введенные значения правильные. Иначе запрашивает повторный ввод.
    :param message: сообщение пользователя
    :return: ничего не возвращает"""
    string = message.text.lower()
    res = add_new_food_base(string)
    if res == 1:
        msg = bot.send_message(message,
                               'Неправильно введены значения. Повторите ввод.')
        bot.register_next_step_handler(msg, add_new_food_step)

    else:
        prod, prot, fat, carboh, ccal = res
        if prod.replace(" ", "").isalpha() and \
                prot.replace(".", "").isnumeric() and fat.replace(".",
                                                                  "").isnumeric() and \
                carboh.replace(".",
                               "").isnumeric() and ccal.isdigit():  # проверка на правильность ввода
            food = FoodClass(prod, prot, fat, carboh,
                             ccal)  # если все правильно, добавляем в базу
            sql = "INSERT INTO products (product, proteins, fats, carbohydrates, calories) \
                                                      VALUES (%s, %s, %s, %s, %s)"
            val = (food.name, food.prot, food.fat, food.carboh, food.ccal)
            cursor.execute(sql, val)
            db.commit()

            # запрашиваем заново название продукта, чтобы вывести его
            msg = bot.send_message(message.chat.id, "База пополнена,"
                                                    " чтобы добавить этот продукт в съеденное,"
                                                    " введите название продукта еще раз. ")
            bot.register_next_step_handler(msg, add_food_step)
        else:  # неправильный ввод названия
            msg = bot.reply_to(message,
                               'Неправильно ввели значения. Повторите.')
            bot.register_next_step_handler(msg, add_food_step)


@bot.callback_query_handler(func=lambda c: c.data == '?')  # нажали кнопку что я съел
def nutritionn(c):
    """Функция обработки кнопки 'Что я съел?'.
    :param c: нажатая кнопка
    :return: ничего не возвращает"""
    markup = types.InlineKeyboardMarkup(row_width=3)
    button_v = types.InlineKeyboardButton("Посмотреть", callback_data='v')
    button_d = types.InlineKeyboardButton("Удалить продукт", callback_data='d')
    button_a = types.InlineKeyboardButton("Удалить все", callback_data='all')
    markup.add(button_v, button_d, button_a)
    bot.send_message(c.message.chat.id, 'Что вы хотите сделать?',
                     reply_markup=markup)


@bot.callback_query_handler(
    func=lambda c: c.data == 'all' or c.data == 'd' or c.data == 'v')
def list_food_action(c):  # разветвление по возможным действиям
    """Функция обработки выбора пользователя.
        Выводит съеденные продукты, при нажатии 'Посмотреть'.
        Запрашивает название удаляемого продукта, при нажатии 'Удалить продукт'.
        Удаляет все съеденные продукты, при нажатии 'Удалить все'.
    :param c: нажатая кнопка
    :return: ничего не возвращает"""
    user_id = c.from_user.id
    if c.data == 'v':
        summp = 0
        summf = 0
        summc = 0
        summcc = 0
        sql = "SELECT `prodn`, `prot`, `fats`, `carbh`, `call` FROM together WHERE user_id LIKE %s "
        val = (user_id,)
        cursor.execute(sql, val)
        res = cursor.fetchall()
        if res:
            for string in res:
                string = ','.join(string)
                n, p, f, cb, cc = string.split(',')
                summp = summp + float(p)
                summf = summf + float(f)
                summc = summc + float(cb)
                summcc = summcc + float(cc)
                bot.send_message(c.message.chat.id, 'вы съели : {0}'.format(n))
        else:
            bot.send_message(c.message.chat.id, 'Вы еще ничего не ели.')
            return

        sql = "SELECT ccal, proteins, fats, carb FROM users WHERE user_id LIKE %s "
        val = (user_id,)
        cursor.execute(sql, val)
        result = cursor.fetchall()
        if result:
            for p in result:
                p = ' '.join(p)
                result = p
            ccal, proteins, fats, carb = result.split()
            bot.send_message(c.message.chat.id,
                             ' всего \n белков: {0} из {5} \n жиров: {1} из {6} \n углеводов: {2} из {7}'
                             ' \n калорий: {3} из {4}'
                             .format(summp, summf, summc, summcc, ccal,
                                     proteins, fats, carb))
        else:  # нет в базе
            bot.send_message(c.message.chat.id, "Ваших данных нет в базе")

    if c.data == 'all':
        sql = "DELETE FROM together WHERE user_id LIKE %s"
        val = (user_id,)
        cursor.execute(sql, val)
        db.commit()
        bot.send_message(c.message.chat.id, 'Все было удалено.')

    if c.data == 'd':
        msg = bot.send_message(c.message.chat.id,
                               'Напишите продукт, который надо удалить')
        bot.register_next_step_handler(msg, delete_list_food)


def delete_list_food(message):  # ветка с удалением продукта
    """Функция для удаления одного продукта.
           Удаляет введенный продукт из базы съеденных продуктов.
    :param message: сообщение пользователя
    :return: ничего не возвращает"""
    try:
        name = message.text.lower()
        user_id = message.from_user.id
        sql = "DELETE FROM together WHERE prodn LIKE %s AND user_id LIKE %s"
        val = (name, user_id,)
        cursor.execute(sql, val)
        db.commit()
        bot.send_message(message.chat.id, 'Продукт был удален.')
    except Exception as e:
        print(e)
        msg = bot.send_message(message.chat.id,
                               'Неправильно введено значение. Повторите ввод.')
        bot.register_next_step_handler(msg, delete_list_food)


@bot.message_handler(content_types=['text'])
def remind(message):
    """
    Функция обрабатывает сообщение пользователя, вызывает другую функцию и
    выводит сообщение
    :param message: сообщение пользователя
    :return: ничего не возвращает
    """
    mes = bot.send_message(message.chat.id,
                           'Напиши дату и время в которое тебе удобно заниматься в формате ДД.ММ.ГГГГ ЧЧ:ММ, '
                           'а я напомню тебе о тренировке \n'
                           'Я работаю в часовом поясе Красноярска. Чтобы изменить часовой пояс, введите'
                           ' timezone')
    bot.register_next_step_handler(mes, timedate)


def timedate(message):
    """
    Функция обрабатывает сообщение пользователя, вызывает другую функцию и
    выводит сообщения и кнопки или добавляет время тренировки и делает
    напоминание
    :param message: сообщение пользователя
    :return: ничего не возвращает
    """
    if message.text == "timezone" or message.text == "Timezone":
        markup = types.InlineKeyboardMarkup(row_width=2)
        button_mos = types.InlineKeyboardButton("Москва", callback_data='M')
        button_kr = types.InlineKeyboardButton("Красноярск", callback_data='K')
        markup.add(button_mos, button_kr)
        bot.send_message(message.chat.id, 'Выберите часовой пояс',
                         reply_markup=markup)

    else:
        timezone = timezone_dict['hour']
        res = input_date(message.text, timezone)

        if res == 'null':
            mes = bot.send_message(message.chat.id,
                                   'Неправильно введены значения. Повторите ввод. В формате ДД.ММ.ГГГГ ЧЧ:ММ')
            bot.register_next_step_handler(mes, timedate)

        else:
            date_user, date_user_, time_user, time_user_, day, month, year, hour, min = input_date(
                message.text,
                timezone)
            bot.send_message(message.chat.id,
                             'Дата тренировки {}.{}.{}. Время {}:{}. Я обязательно напомню.'
                             .format(day, month, year, hour, min))
            if timezone == 4:
                res = proсess_date(date_user_, time_user_)
            else:
                res = proсess_date(date_user, time_user)

            if res == -1:
                mes = bot.send_message(message.chat.id,
                                       'Я тут подумал, но ведь этот день уже прошел. Введите дату заново.')
                bot.register_next_step_handler(mes, timedate)

            if res == 1:
                bot.send_message(message.chat.id,
                                 'Ты же не забыл о тренировке?')


@bot.callback_query_handler(func=lambda c: c.data == 'M' or c.data == 'K')
def timezone(c):
    """
    Функция обрабатывает сообщение пользователя, меняет часовой пояс,
    выводит сообщения
    :param c: нажатая кнопка
    :return: ничего не возвращает
    """
    if c.data == 'M':
        hour_zone = 4
        mes = bot.send_message(c.message.chat.id,
                               'Часовой пояс - Москва. Напиши дату и время,'
                               ' в которое тебе удобно заниматься в формате ДД.ММ.ГГ. ЧЧ:ММ')
    else:
        hour_zone = 0
        mes = bot.send_message(c.message.chat.id,
                               'Часовой пояс - Красноярск. Напиши дату и время,'
                               ' в которое тебе удобно заниматься в формате ДД.ММ.ГГ. ЧЧ:ММ')
    timezone_dict['hour'] = hour_zone
    bot.register_next_step_handler(mes, timedate)


@bot.message_handler(content_types=['text'])
def list_exercises_today(message):
    """Функция вывода сегодняшних упражнений.
    Выводит упражнения, добавленные пользоватлем.
    :param message: сообщение пользователя
    :return: ничего не возвращает"""
    user_id = message.from_user.id
    excrs, ex_list = view_exercises(user_id)
    if excrs != 0:
        bot.send_message(message.chat.id, "\n".join(ex_list))
        msg = bot.send_message(message.chat.id,
                               "Надо ли удалить все упражнения из файла(да/нет)?")
        bot.register_next_step_handler(msg, delete_excers)
    else:
        bot.send_message(message.chat.id, "Упражнения еще не добавлены")




bot.polling()
