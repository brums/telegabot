def input_parameters(message):
    """
    Функция обрабатывает сообщение пользователя, вызывает другую функцию и
    выводит сообщение, проверяет корректность введенных значений
    :param message: сообщение пользователя
    :return: ничего не возвращает    """
    try:
        param = message
        height, weight, age = param.split(',')
        res = 0

        if height.isdigit():
            height = int(height)

        if weight.isdigit():
            weight = int(weight)

        if age.isdigit():
            age = int(age)

    except Exception as e:
        res = 1
        return res

    else:
        return int(height), int(weight), int(age)


def calc(val, height, weight, age, gen, activity):
    """
    Функция обрабатывает сообщение пользователя, расчитывает КБЖУ
    :param val: значение
    :param height: рост
    :param weight: вес
    :param age: возраст
    :param gen: пол
    :return: нужный КБЖУ
    """
    all_ccal = int(((9.8 * weight) + (6.25 * height) - (5 * age) + gen) * activity)
    if val == 1:
        all_ccal = int(all_ccal + 0.15 * all_ccal)
        proteins = int(all_ccal * 0.35 / 4)
        fats = int(all_ccal * 0.17 / 9)
        carb = int(all_ccal * 0.48 / 4)

    if val == 2:
        all_ccal = int(all_ccal - 0.15 * all_ccal)
        proteins = int(all_ccal * 0.4 / 4)
        fats = int(all_ccal * 0.3 / 9)
        carb = int(all_ccal * 0.3 / 4)

    if val == 3:
        all_ccal = all_ccal
        proteins = int(all_ccal * 0.35 / 4)
        fats = int(all_ccal * 0.3 / 9)
        carb = int(all_ccal * 0.35 / 4)
    return all_ccal, proteins, fats, carb
