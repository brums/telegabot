from db_connection import connect
import math

cursor, db = connect()


def search_food(product):
    """Функция поиска продукта в базе.
    :param product: введенный продукт
    :return: информация о продукте или 0, если продукта нет"""
    sql = "SELECT product, proteins, fats, carbohydrates, calories FROM products WHERE product LIKE %s "
    val = (product,)
    cursor.execute(sql, val)
    products = cursor.fetchall()
    if products:  # если продукт есть в базе, выводим его
        for p in products:
            p = ','.join(p)
            products = p
    else:
        products = 0
    return products


def count_ccal(name, prot, fat, carboh, ccal, gr):
    """Функция рассчета КБЖУ на кол-во съеденных грамм.
        :param name: название продукта.
        :param prot: протеины.
        :param fat: жиры.
        :param carboh: углеводы.
        :param ccal: калории.
        :param gr: граммовка.
        :return: посчитанные значения"""
    prot = math.ceil((float(prot.replace(',', ".")) / 100) * gr)
    fat = math.ceil((float(fat.replace(',', ".")) / 100) * gr)
    carboh = math.ceil((float(carboh.replace(',', ".")) / 100) * gr)
    ccal = math.ceil((float(ccal.replace(',', ".")) / 100) * gr)
    return name, prot, fat, carboh, ccal


def add_new_food_base(string):
    """Функция преобразования строки.
       :param string: введенная строка с продуктом.
       :return: преобразованные значения, 0, если недостаточно значений, 1 в другом случае"""
    try:
        string.replace(" ", "")
        prod, prot, fat, carboh, ccal = string.split(',')

    except Exception as e:  # если недостаточно значений
        res = 1
        return res

    else:
        prod, prot, fat, carboh, ccal = string.split(',')
        return prod, prot, fat, carboh, ccal


def output_excers(sql):
    """Функция преобразования результата запроса в набор упражнений.
        :param sql: запрос в базу
        :return: список упражнений"""
    cursor.execute(sql)
    excer = cursor.fetchall()
    ex_list = []
    i = 1
    for exc in excer:
        exc = ''.join(exc)
        ex_list.append(str(i) + ") " + exc)
        i += 1
    return ex_list


def exercise(exer):
    """Функция вывода описания упражнения по запросу.
    :param exer: название
    :return: описание упражнения"""
    sql = "SELECT definition FROM exercises WHERE title = %s "
    val = (exer,)
    cursor.execute(sql, val)
    excr = cursor.fetchall()
    return excr


def add_exercise(text):
    """Функция проверки корректного ввода подходов и повторений.
    :param text: строка с кол-вом подходов и повторений
    :return: подходы и повторения, 1 в противном случае"""
    try:
        amount, repeat = text.split(",")
        int(amount)
        int(repeat)
    except Exception:
        res = 1
        return res
    else:
        return amount, repeat


def view_exercises(user_id):
    """Функция преобразования результата запроса в набор персональных упражнений на сегодня.
        :param user_id: id пользователя
        :return: список упражнений на сегодня"""
    sql = "SELECT exc FROM personal_exc WHERE user_id LIKE %s "
    val = (user_id,)
    cursor.execute(sql, val)
    excrs = cursor.fetchall()
    if excrs:
        ex_list = []
        for exc in excrs:
            exc = ''.join(exc)
            ex_list.append(exc)
        return excrs, ex_list


def output_recepy(sql):
    """Функция преобразования результата запроса в список с названием блюд.
        :param sql: запрос в базу
        :return: список блюд"""
    cursor.execute(sql)
    rec = cursor.fetchall()
    rec_list = []
    i = 1
    for recp in rec:
        recp = ''.join(recp)
        rec_list.append(str(i) + ") " + recp)
        i += 1
    return rec_list


def name_recp(recep):
    """Функция  получения рецепта, введенного блюда.
        :param recep: блюдо
        :return: рецепт"""
    sql = "SELECT deftn FROM recepies WHERE name = %s "
    val = (recep,)
    cursor.execute(sql, val)
    recp = cursor.fetchall()
    return recp


def name_ingrds(recep):
    """Функция  получения ингридиентов, введенного блюда.
        :param recep: блюдо
        :return: список ингридиентов"""
    sql = "SELECT ingrds FROM recepies WHERE name = %s "
    val = (recep,)
    cursor.execute(sql, val)
    rcp = cursor.fetchall()
    return rcp
