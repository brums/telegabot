# coding=utf-8
import mysql.connector
from mysql.connector import errorcode


def connect():
    """Функция  подключния к базе.
        :return: соединение с базой"""
    try:  # подключение к базе
        db = mysql.connector.connect(
            host='us-cdbr-iron-east-01.cleardb.net',
            user='bdb1efab19195a',
            passwd='44378465',
            database='heroku_03587e929a0dd14',
            charset="utf8"
        )

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)

    else:
        cursor = db.cursor()
        return cursor, db


cursor, db = connect()
