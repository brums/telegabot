# -*-coding: utf-8-*-
import db_connection

cursor, db = db_connection.connect()

class FoodClass:  # продукт для внесения в базу
    def __init__(self, prod, prot, fat, carboh, ccal):
        self.name = prod
        self.prot = prot
        self.fat = fat
        self.carboh = carboh
        self.ccal = ccal


def food(input):
    if input.isalpha():
        sql = "SELECT product FROM products WHERE product LIKE %s "
        val = (input.lower(),)
        cursor.execute(sql, val)
        products = cursor.fetchall()
        if products:  # если продукт есть в базе, выводим его
            for p in products:
                p = ','.join(p)
                return p
    return None


def add_food(input):
    try:
        string = input.lower()
        string.replace(" ", "")
        prod, prot, fat, carboh, ccal = string.split(',')
    except Exception:  # если недостаточно значений
        return 1

    else:
        if prod.replace(" ", "").isalpha() and \
                prot.replace(".", "").isnumeric() and fat.replace(".",
                                                                  "").isnumeric() and \
                carboh.replace(".",
                               "").isnumeric() and ccal.isdigit():  # проверка на правильность ввода
            fd = FoodClass(prod, prot, fat, carboh,
                           ccal)  # если все правильно, добавляем в базу
            sql = "INSERT INTO products (product, proteins, fats, carbohydrates, calories) \
                                                     VALUES (%s, %s, %s, %s, %s)"
            val = (fd.name, fd.prot, fd.fat, fd.carboh, fd.ccal)
            cursor.execute(sql, val)
            db.commit()
            return fd.name
        else:
            return None


def exercise(input):
    sql = "SELECT definition FROM exercises WHERE title = %s "
    val = (input.lower(),)
    cursor.execute(sql, val)
    excr = cursor.fetchall()
    if excr:
        for e in excr:
            e = ''.join(e)
            e = e.replace('\r\n', '')
            e = e.replace('\n', '')
        return e
    return None
