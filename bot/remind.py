import telebot
from telebot import types
import datetime
import time
import threading


def input_date(message, timezone):
    """
    Функция обрабатывает сообщение пользователя, добавляет напоминание
    :param message: сообщение пользователя
    :param timezone: часовой пояс
    :return: Если выполняется исключение: res
            Иначе: date_user, date_user_, time_user, time_user_, day, month, year,
            hour, min
    """
    try:
        date_user_ = 0
        time_user_ = 0
        date_time = message
        date, time = date_time.split(' ')
        day, month, year = date.split('.')
        hour, min = time.split(':')
        hour = int(hour)
        min = int(min)
        now_time = datetime.datetime.now().time()

        if timezone == 4:
            hour_ = int(hour)
            day_ = int(day)
            if hour_ >= 20 and hour_ <= 23:
                hour_ = hour_ - 20
                day_ = day_ + 1

            else:
                hour_ = hour_ + timezone

            time_user_ = now_time.replace(hour=hour_, minute=min, second=0,
                                          microsecond=0)
            date_user_ = str(year + '.' + month + '.' + str(day_))
            date_user_ = date_user_.replace('.', '-')

        now_time = datetime.datetime.now().time()
        time_user = now_time.replace(hour=hour, minute=min, second=0,
                                     microsecond=0)
        date_user = str(year + '.' + month + '.' + day)
        date_user = date_user.replace('.', '-')

    except Exception as e:
        print(e)
        res = 'null'
        return res

    else:
        return date_user, date_user_, time_user, time_user_, day, month, year, hour, min


def proсess_date(date_user, time_user):
    """
    Функция обрабатывает дату и время, введенные пользователем, если все
    хорошо, то ждет наступления даты
    :param date_user: дата
    :param time_user: время
    :return: Если выполняется исключение: res
    """
    now_time = datetime.datetime.now().time()
    now_date = datetime.datetime.now().date()
    now_date = str(now_date)
    if now_date > date_user:
        res = -1
        return res

    while True:
        res = 0
        now_time = datetime.datetime.now().time()
        now_date = datetime.datetime.now().date()
        now_date = str(now_date)
        if now_date == date_user:
            if now_time > time_user:
                t = threading.Timer(5.0, timedate)
                t.start()
                t.join()
                res = 1
                return res
                break
        if res == 0:
            time.sleep(30)


def timedate():
    """
    Функция печатает "ок"
    """
    print('ok')
